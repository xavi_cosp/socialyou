<?php 
	$servername = "localhost";
	$username = "testapi";
	$password = "testapi";
	$database = "testapi_ddbb";
	$url = $_SERVER['REQUEST_URI'];

	$params = explode("/",$url);

	// Check url format
	if(sizeof($params) < 3 || ($params[1] != 'addmovie' && $params[1] != 'removemovie')){
		die("The url provided is not correct.");
	}else if($params[1] == 'addmovie'){
		if(sizeof($params) < 4){
			die("The url provided is not correct.");
		}else if($params[1] == '' || $params[2] == '' || $params[3] == ''){
			die("Invalid parameters passed");
		}
	}else if($params[1] == 'removemovie'){
		if($params[1] == '' || $params[2] == ''){
			die("Invalid parameters passed");
		}else if(sizeof($params) > 3){
			if($params[3] != ''){
				die("Too much parameters passed");
			}
		}
	}else{
		die("Action does not exist. Use addmove or removemovie instead.");
	}
	$action = $params[1];
	$title = str_replace('%20', ' ', $params[2]);
	if($params[1] == 'addmovie'){
		$year = (int)$params[3]; 
		if($year>1000 && $year<2100)
	    {
	      $released_year = $params[3]; 
	    }else{
	    	die("Year not valid");
	    }	
	}

	// Create connection
	$conn = new mysqli($servername, $username, $password, $database);
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	}
	//add movie
	if($action == 'addmovie'){
		$query1 = 'SELECT id FROM movies WHERE title ="'.$title.'"';
		$result = $conn->query($query1);
		//check if exist
		if ($result->num_rows > 0) {
			die("This movie already exist in the database.");
		}else{
			// insert into database
			$query2 = 'INSERT INTO movies (title, release_year) VALUES ("'.$title.'",'.$released_year.')';
			if ($conn->query($query2) === TRUE) {
			    echo "New movie created successfully";
			} else {
			    die("Error: " . $query2 . "<br>" . $conn->error);
			}
		}
	}
	else{ // delete movie
		// check if exist
		$query1 = 'SELECT id FROM movies WHERE title ="'.$title.'"';
		$result = $conn->query($query1);
		if ($result->num_rows > 0) {
			// delete from database
			$query2 = 'DELETE FROM movies WHERE title ="'.$title.'"';
			if ($conn->query($query2) === TRUE) {
			    echo "Movie deleted successfully";
			} else {
			    die("Error: " . $query2 . "<br>" . $conn->error);
			}
			
		}else{
			die("This movie does not exist.");
		}
	}
	
 ?>
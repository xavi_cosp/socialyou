

function model(operation, data, schema){
	var DB = [];
	var i = 0;
	var obj = {};

	// Check Data
	for (var key in data) {
		if (key == schema[i]) {
			obj[key] = schema[i];
		}
	        i++;
	}

	// If object is empty, return
	if(Object.getOwnPropertyNames(obj).length === 0){
		return;
	}

	// Add object to DB id operation is add
	if(operation == "add"){
		DB.push(obj);
	}
	
}